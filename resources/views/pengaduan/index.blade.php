@extends('template.master-user')
@section('title')
Data Pengaduan 
@endsection
@section('content')
<div class="card">
    <!-- /.card-header -->
    <div class="card-body">
        <a class="btn btn-primary" href="Pengaduan/create" role="button">Tambah Data</a>    
<br>
<br>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No.</th>
          <th>Tanggal</th>
          <th>NIK</th>
          <th>Isi Pengaduan</th>
          {{-- <th>Foto</th> --}}
          <th>Status</th>
          <th>ID Masyarakat</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($Pengaduan as $key => $item)
          {{-- {{$item->genre->nama}}   --}}
        <tr>
            <th>{{$key+4}}</th>
            <td>{{$item->tgl_pengaduan}}</td>
            <td>{{$item->NIK}}</td>
            <td>{{$item->isi_pengaduan}}</td>
            {{-- <td>
              <img src ="{{asset('gambar/'.$item->foto)}}" alt="Image" height="20" width="20" >
            </td> --}}
            <td>{{$item->status}}</td>
            <td>{{$item->users_id}}</td>
            <td>
                <form action="/Pengaduan/{{$item->id}}" method="POST">
                <a href="/Pengaduan/{{$item->id}}" class="btn btn-info btn-sm"> Detail</a>
                <a href="/Pengaduan/{{$item->id}}/edit" class="btn btn-warning btn-sm"> Edit</a>
                
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
      </tr>
        @empty
            <tr>
                Data Tanggapan Masih Kosong
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
<!-- /.card-body -->
</div>
@endsection