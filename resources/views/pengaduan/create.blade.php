@extends('template.master-user')
@section('topbar')    
@endsection
@section('title')
<h1>form pengaduan</h1>    
@endsection
@section('content')

    <form method="post" action="/Pengaduan" enctype="multipart/form-data">
        @csrf
        {{-- <label name="users_id" value="{{$Masyarakat->id}}" > nama : {{$Masyarakat->name}}</label> --}}
      

        <div class="form-group" >
          <label >Tanggal</label>
          <input type="date" class="form-control" name="tgl_pengaduan"  >
        </div>
        @error('tgl_pengaduan')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label >NIK </label>
            <input type="text" class="form-control" name="NIK" >
        </div>
        @error('NIK')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        
        <div class="form-group">
            <label >Isi Pengaduan </label>
            <textarea name="isi_pengaduan" class="form-control" cols="30" rows="10"  > </textarea>
        </div>
        @error('isi_peengaduan')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <div class="form-group">
          <label >foto</label>
          <input type ='file' class="form-control" name="foto"> 
      </div>
      @error('foto')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>   
@endsection