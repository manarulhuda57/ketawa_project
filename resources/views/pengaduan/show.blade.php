@extends('template.master-user')
@section('topbar')    
@endsection
@section('title')
<h1>detail pengaduan</h1>    
@endsection
@section('content')
  {{-- <img src ="{{asset('gambar/'.$Pengaduan->foto)}}" alt="Image" height="290" width="200" > <br>
  <p>Tanggal Pengaduan : {{$Pengaduan->tgl_pengaduan}}</p> <br><br>
 <br> <p>Isi Pengaduan : {{$Pengaduan->isi_pengaduan}}</p> --}}

 <div class="card" style="width: 18rem;">
    <img class="card-img-top" src="{{asset('gambar/'.$show->foto)}}" alt="Card image cap">
    <div class="card-body">
      <p>Tanggal Pengaduan : {{$show->tgl_pengaduan}}</p>
      <p class="card-text">{{$show->isi_pengaduan}}</p>
      <a href="/pengaduan" class="btn btn-primary" > Kembali </a>
    </div>
  </div>
@endsection