@extends('template.master-admin')
@section('title')
<h1>Halaman Aduan</h1>
@endsection
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.0/css/dataTables.bootstrap4.min.css"> 

@endpush

@push('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.0/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function () {
        $('#tabel').DataTable();
    });
    </script>
@endpush
@section('content')




<table id="tabel" class="table table-striped table-bordered" style="width:100%">
   <thead>
    <tr>
        <th style="width: 30px; text-align:center;">No.</th>
        <th style="width: 150px;" class="text-center">Foto</th>
        <th style="width: 100px;" class="text-center">NIK</th>
        <th style="width: 300px;" class="text-center">Isi Aduan</th>
        <th style="width: 50px;" class="text-center">Status</th>
        <th class="text-center">Aksi</th>

    </tr>
   </thead>
    <tbody>


        @forelse ($pengaduan as $key=> $aduan)
            <tr>
                <td>{{$key+1}}</td> 
                <td>{{$aduan->foto}}</td> 
                <td>{{$aduan->NIK}}</td>
                <td>{{$aduan->isi_pengaduan}}</td> 
                <td>{{$aduan->status}}</td> 
         
            <form action="/pengaduan/{{$aduan->id}}" method="POST">
                <td>
                    <a href="/pengaduan/{{$aduan->id}}" class="btn btn-primary">Detail</a>
                    <a href="/pengaduan/{{$aduan->id}}/edit" class="btn btn-success">Tanggapi</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger" value="Delete">
                </form>
                </td>
            </tr>
            
                @empty
                <tr>
                    <td>Data Kategori Masih Kosong</td>
                </tr>
                @endforelse

    </tbody>
            
</table>



@endsection