@extends('layout.master')
@section('title')
Halaman Edit 
@endsection
@section('content')
<form method="POST" action="/tanggapan/{{$cast->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Tanggal</label>
      <input type="date" class="form-control" name="tgl_tanggapan">
    </div>
    @error('tgl_tanggapan')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label>Tanggapan</label>
      <input type="text" class="form-control" name="tanggapan">
    </div>
    @error('tanggapan')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
{{-- <div class="form-group">
  <label>ID Petugas</label>
  <input type="text" class="form-control" value="{{$tanggapan->petugas_id}}" name="petugas_id">
</div>
<div class="form-group">
  <label>ID Pengaduan</label>
  <input type="text" class="form-control" value="{{$tanggapan->pengaduan_id}}" name="pengaduan_id">
</div> --}}
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection