@extends('template.master-user')
@section('title')
Data Tanggapan 
@endsection
@section('content')
<div class="card">
    <!-- /.card-header -->
    <div class="card-body">
        <a class="btn btn-primary" href="/tanggapan/create" role="button">Tambah Data</a>    
<br>
<br>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No.</th>
          <th>Tanggal</th>
          <th>Tanggapan</th>
          {{-- <th>ID Petugas</th>
          <th>ID Pengaduan</th> --}}
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($tanggapan as $item)
          {{-- {{$item->genre->nama}}   --}}
        <tr>
        <td>{{$item->tgl_tanggapan}}
        </td>
        <td>{{$item->tanggapan}}</td>
        {{-- <td>{{$item->petugas_id}}</td>
        <td>{{$item->pengaduan_id}}</td> --}}
        <td>
            <form action="/tanggapan/{{$item->id}}" method="POST">
            <a href="/tanggapan/{{$item->id}}" class="btn btn-info btn-sm"> Detail</a>
            <a href="/tanggapan/{{$item->id}}/edit" class="btn btn-warning btn-sm"> Edit</a>
            
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
        </td>
      </tr>
        @empty
            <tr>
                Data Tanggapan Masih Kosong
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
<!-- /.card-body -->
</div>
@endsection