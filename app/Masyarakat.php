<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Masyarakat extends Model
{
    protected $table = "masyarakat";
    protected $fillable = ["name", "email", "password", ];
}
