<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


//use Illuminate\Support\Facades\DB;
//use File;
use App\Pengaduan;
use App\Masyarakat;
use App\User;



class PengaduanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Pengaduan = Pengaduan::all();
        return view('pengaduan.index',compact('Pengaduan'));
        // return View('page,index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $Masyarakat = user::all();
        return view('pengaduan.create',compact('Masyarakat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'tgl_pengaduan' => 'required',
            'NIK' => 'required|min:10',
            'isi_pengaduan'  => 'required',
            'foto' => 'required|mimes:jpeg,png,jpg|max:2048'
        ],
       
        );

        $imageName = time().'.'.$request->foto->extension();  
        $request->foto->move(public_path('gambar'), $imageName);

        $Pengaduan = new pengaduan;
        $Pengaduan->tgl_pengaduan = $request->tgl_pengaduan;
        $Pengaduan->NIK = $request->NIK;
        $Pengaduan->isi_pengaduan = $request->isi_pengaduan;
        $Pengaduan->foto = $imageName;
        $Pengaduan->status = '0';
        $Pengaduan->users_id = '1'; // $request->users_id;
        $Pengaduan->save();
    
        return redirect('/Pengaduan');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $show = Pengaduan::find($id);
 
       
        return view('pengaduan.show',compact('show'));

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Pengaduan = Pengaduan::findorfail($id);
        $Pengaduan->delete();


        return redirect('/Pengaduan');



    }
}
