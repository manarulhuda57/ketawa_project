<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Tanggapan;

class TanggapanController extends Controller
{
    public function index()
    {
        $tanggapan = DB::table('tanggapan')->get();
        return view('tanggapan.index', compact('tanggapan'));
    }
    public function create()
    {
        return view('tanggapan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'tgl_tanggapan' => 'required',
            'tanggapan' => 'required',
            'petugas_id' => 'required',
            'pengaduan_id' => 'required',

        ]);
        DB::table('tanggapan')->insert(
            [
                'tgl_tanggapan' => $request['tgl_tanggapan'], 
                'tanggapan' => $request['tanggapan'],
                'petugas_id' => $request['petugas_id'],
                'pengaduan_id' => $request['pengaduan_id']
                ]
        );
        return redirect('/tanggapan');
    }

    public function show($id)
    {
        $tanggapan = DB::table('tanggapan')->where('id', $id)->first();
        return view('tanggapan.show', compact('tanggapan'));
    }

    public function edit($id)
    {
        $tanggapan = DB::table('tanggapan')->where('id', $id)->first();
        return view('tanggapan.edit', compact('tanggapan'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'tgl_tanggapan' => 'required',
            'tanggapan' => 'required',
        ]);
        DB::table('tanggapan')
              ->where('id', $id)
              ->update(
                  [
                  'tgl_tanggapan' => $request['tgl_tanggapan'],
                  'tanggapan' => $request['tanggapan']
                ]
            );
            return redirect('/tanggapan');
    }

    public function destroy($id)
    {
        DB::table('tanggapan')->where('id', $id)->delete();
        return redirect('/tanggapan');
    }
}
