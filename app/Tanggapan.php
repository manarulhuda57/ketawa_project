<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tanggapan extends Model
{
   protected $table = 'tanggapan';
   protected $fillable = ['tgl_tanggapan','tanggapan', 'pengaduan_id', 'petugas_id'];

   public function pengaduan()
   {
      return $this->belongsTo('App\Pengaduan', 'pengaduan_id');
   }

   public function petugas()
   {
      return $this->belongsTo('App\Petugas', 'petugas_id');
   }
}
