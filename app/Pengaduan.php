<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengaduan extends Model
{
    protected $table = 'pengaduan';
    protected $fillable = ['tgl_pengaduan', 'NIK', 'isi_pengaduan', 'foto', 'status', 'users_id'];

    public function tanggapan()
    {
        return $this->hasMany('App\Tanggapan');
    }
}
