<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {
    return view('page_admin.index');
});

Route::get('/aduan', 'PengaduanController@index');

Route::get('/tanggapan', 'TanggapanController@index');


// Ini route untuk halaman utama Masyarakat
// Route::get('/', 'MasyarakatController@index');

// Ini route untuk halaman login
Route::get('/login', 'LoginController@index');

// Ini route untuk halaman pengaduan
Route::resource('Pengaduan','PengaduanController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Tanggapan
Route::get('/tanggapan', 'TanggapanController@index');
Route::get('/tanggapan/create', 'TanggapanController@create');
Route::post('/tanggapan', 'TanggapanController@store');
Route::get('/tanggapan/{tanggapan_id}', 'TanggapanController@show');
Route::get('/tanggapan/{tanggapan_id}/edit', 'TanggapanController@edit');
Route::put('/tanggapan/{tanggapan_id}', 'TanggapanController@update');
Route::delete('/tanggapan/{tanggapan_id}', 'TanggapanController@destroy');

//Pengaduan
Route::get('/pengaduan', 'PengaduanController@index');